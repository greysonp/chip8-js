module.exports = {
    entry: "./src/index.js",
    devtool: "cheap-module-source-map",
    output: {
        path: __dirname,
        filename: "bundle.js",
        devtoolLineToLine: true,
        sourceMapFilename: "bundle.js.map"
    },
    module: {
        rules: [{
            test: /\.scss$/,
            use: [{
                loader: "style-loader" // creates style nodes from JS strings
            }, {
                loader: "css-loader" // translates CSS into CommonJS
            }, {
                loader: "sass-loader" // compiles Sass to CSS
            }]
        },
        { 
            test: /\.(png|woff|woff2|eot|ttf|svg)$/, 
            loader: 'url-loader?limit=100000' 
        }]
    }
};
