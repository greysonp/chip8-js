var assert = require('assert');
var Cpu = require('../src/Cpu');
var seedrandom = require('seedrandom');
var rng = require('../src/util/RandomByte');
var util = require('../src/util/Util');

describe('Cpu', function() {
  describe('#load()', function() {
    it('Copies the provided program into memory.', function() {
      var cpu = new Cpu();
      var program = new Uint8Array([0x01, 0x02, 0x03, 0x04])
      cpu.load(program);

      assert.deepEqual(cpu.memory.slice(0x200, 0x200 + 4), program);
    });

    it('Never expands beyond 4096 instructions.', function() {
      var cpu = new Cpu();
      var program = new Uint8Array(new Array(10000));
      cpu.load(program);

      assert.deepEqual(cpu.memory.length, 4096);
    });
  });

  describe('#updateTimers()', function() {
    var cpu = new Cpu();
    cpu.delayTimer = 1;
    cpu.soundTimer = 1;

    cpu.updateTimers();

    it('Delay timer decrements.', function() {
        assert.equal(cpu.delayTimer, 0);
    });

    it('Sound timer decrements.', function() {
        assert.equal(cpu.soundTimer, 0);
    });

    cpu.updateTimers();

    it('Delay timer does not fall below zero.', function() {
        assert.equal(cpu.delayTimer, 0);
    });

    it('Sound timer does not fall below zero.', function() {
        assert.equal(cpu.soundTimer, 0);
    });
  });

  describe('#cycle()', function() {
    it('Runs the instruction pointed to by the program counter.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x60, 0x01,
        0x60, 0x02,
        0x60, 0x03
      ]));

      cpu.pc = 0x204;
      cpu.cycle();
      assert.equal(cpu.registers[0], 3);

      cpu.pc = 0x200;
      cpu.cycle();
      assert.equal(cpu.registers[0], 1);
    });

    describe('0x00E0 clears the screen.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x00, 0xe0,
        0x00, 0xe0
      ]));

      var buffer = cpu.getVideoBuffer();

      it('Clears the screen when it\'s full.', function() {
        // Fill in buffer completely
        util.fillArray(buffer, true);

        // Clear the screen
        cpu.cycle();

        // Assert all values are cleared
        for (var i = 0; i < buffer.length; i++) {
          assert(!cpu.getVideoBuffer[i]);
        }
      });

      it('Leaves it unchanged when it\'s empty.', function() {
        cpu.cycle();

        // Assert all values are still cleared
        for (var i = 0; i < buffer.length; i++) {
          assert(!cpu.getVideoBuffer[i]);
        }
      });
    });

    describe('0x00EE returns from a subroutine.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x22, 0x04,
        0x00, 0xe0,
        0x00, 0x00,
        0x00, 0xee
      ]));
      assert.equal(cpu.sp, 0);

      it('Jumps to the correct address when calling the subroutine.', function() {
        cpu.cycle();
        assert.equal(cpu.pc, 0x204);
      });

      it('Updates the stack correctly when calling the subroutine.', function() {
        assert.equal(cpu.sp, 1);
        assert.equal(cpu.stack[0], 0x202);
      });

      it('Subroutine executes properly.', function() {
        cpu.cycle();
        assert.equal(cpu.pc, 0x206);
      });

      it('Jumps back to the correct address when returning.', function() {
        cpu.cycle();
        assert.equal(cpu.pc, 0x202);
      })

      it('Updates the stack correctly upon returning.', function() {
        assert.equal(cpu.sp, 0);
      });

      it('Continues program execution properly after returning.', function() {
        cpu.cycle();
        assert.equal(cpu.pc, 0x204);
      });
    });

    describe('0x1NNN jumps to address NNN.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x22, 0x04,
        0x00, 0x00,
        0x00, 0xe0
      ]));

      it('Jumps to the correct address.', function() {
        cpu.cycle();
        assert.equal(cpu.pc, 0x204);
      });
    });

    describe('0x2NNN calls a subroutine at address NNN.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x22, 0x04,
        0x60, 0x02,
        0x60, 0x01,
      ]));

      // Ensure we go to the correct address
      it('Jumps to the correct address.', function() {
        cpu.cycle();
        assert.equal(cpu.pc, 0x204);
      });

      it('Updates the stack correctly.', function() {
        assert.equal(cpu.sp, 1);
        assert.equal(cpu.stack[0], 0x202);
      });

      it('Subroutine executes properly.', function() {
        cpu.cycle();
        assert.equal(cpu.registers[0], 1);
      });
    });

    describe('0x3XNN skips the next instruction if VX equals NN.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x60, 0x01,
        0x30, 0x00,
        0x00, 0x00,
        0x30, 0x01,
        0x00, 0x00,
        0x00, 0xe0
      ]));

      it('Doesn\'t skip when the value at VX doesn\'t equal NN.', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.pc, 0x204);
      });

      it('Skips when the value at VX equals NN.', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.pc, 0x20a);
      });
    });

    describe('0x4XNN skips the next instruction if VX doesn\'t equal NN.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x60, 0x01,
        0x40, 0x01,
        0x00, 0x00,
        0x40, 0x00,
        0x00, 0x00,
        0x00, 0xe0
      ]));

      it('Doesn\'t skip when the value at VX equals NN.', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.pc, 0x204);
      });

      it('Skips when the value at VX doesn\'t equal NN.', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.pc, 0x20a);
      });
    });

    describe('0x5XY0 skips the next instruction of VX equals VY.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x60, 0x01,
        0x50, 0x10,
        0x61, 0x01,
        0x50, 0x10,
        0x00, 0x00,
        0x00, 0xe0
      ]));

      it('Doesn\'t skip when the value at VX doesn\'t equal VY', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.pc, 0x204);
      });

      it('Skips when the value at VX equals VY.', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.pc, 0x20a);
      });
    });

    describe('0x6XNN sets VX to NN.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x60, 0x01
      ]));

      it('Sets the value.', function() {
        assert.equal(cpu.registers[0], 0);
        cpu.cycle();
        assert.equal(cpu.registers[0], 1);
      });
    });

    describe('0x7XNN adds NN to VX.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x70, 0x01,
        0x70, 0x01,
        0x70, 0xff
      ]));

      it('Adds correctly.', function() {
        assert.equal(cpu.registers[0], 0);
        cpu.cycle();
        assert.equal(cpu.registers[0], 1);
      });

      it('Handles repeated adds to the same register correctly.', function() {
        cpu.cycle();
        assert.equal(cpu.registers[0], 2);
      });

      it('Wraps around (upon going over 255).', function() {
        cpu.cycle();
        assert.equal(cpu.registers[0], 1);
      });

      it('Does not set the carry flag when overflowing.', function() {
        assert.equal(cpu.registers[0xf], 0);
      });
    });

    describe('0x8XY0 sets VX to the value of VY.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x61, 0x01,
        0x80, 0x10
      ]));

      it('Assigns value correctly.', function() {
        assert.equal(cpu.registers[0], 0);
        ncycle(cpu, 2);
        assert.equal(cpu.registers[0], 1);
      });
    });

    describe('0x8XY1 sets VX to VX | VY.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x60, 0xa0, // 10100000
        0x61, 0xc0, // 11000000
        0x80, 0x11  // 11100000 0xe0
      ]));

      it('Performs OR correctly.', function() {
        ncycle(cpu, 3);
        assert.equal(cpu.registers[0], 0xe0);
      });
    });

    describe('0x8XY2 sets VX to VX & VY.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x60, 0xa0, // 10100000
        0x61, 0xc0, // 11000000
        0x80, 0x12  // 10000000 0x80
      ]));

      it('Performs AND correctly.', function() {
        ncycle(cpu, 3);
        assert.equal(cpu.registers[0], 0x80);
      });
    });

    describe('0x8XY3 sets VX to VX ^ VY.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x60, 0xa0, // 10100000
        0x61, 0xc0, // 11000000
        0x80, 0x13  // 01100000 0x60
      ]));

      it('Performs XOR correctly.', function() {
        ncycle(cpu, 3);
        assert.equal(cpu.registers[0], 0x60);
      });
    });

    describe('0x8XY4 adds VY to VX and updates carry.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x61, 0x01,
        0x80, 0x14,
        0x61, 0xff,
        0x80, 0x14
      ]));

      it('Adds correctly without a carry.', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.registers[0], 1);
        assert.equal(cpu.registers[0xf], 0);
      });

      it('Adds correclty with a carry.', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.registers[0], 0);
        assert.equal(cpu.registers[0xf], 1);
      });
    });

    describe('0x8XY5 subtracts VY from VX and updates carry upon borrowing.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x61, 0x01,
        0x80, 0x15,
        0x80, 0x15
      ]));

      it('Subtracts correctly with a borrow.', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.registers[0], 255);
        assert.equal(cpu.registers[0xf], 0);
      });

      it('Subtracts correctly without a borrow.', function() {
        cpu.cycle();
        assert.equal(cpu.registers[0], 254);
        assert.equal(cpu.registers[0xf], 1);
      });
    });

    describe('0x8XY6 shifts VX right by one, sets carry to least significant bit.', function() {
      // NOTE: Old implementations of the CHIP-8 used to use VY, but not anymore
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x60, 0x82, // 10000010
        0x80, 0x06, // 01000001 0x41, no carry
        0x60, 0x83, // 10000011
        0x80, 0x06, // 01000001 0x41, carry
      ]));

      it('Shifts correctly when the lest significant bit is 0.', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.registers[0], 0x41);
        assert.equal(cpu.registers[0xf], 0);
      });

      it('Shifts correctly when the lest significant bit is 1.', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.registers[0], 0x41);
        assert.equal(cpu.registers[0xf], 1);
      });
    });

    describe('0x8XY7 sets VX to VY minux VX and updates carry upon borrowing.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x60, 0x01, // V0 := 1
        0x80, 0x17, // V0 := V1 - V0 = 255
        0x60, 0x01, // V0 := 1
        0x61, 0x03, // V1 := 3
        0x80, 0x17  // V0 := V1 - V0 = 2
      ]));

      it('Subtracts correctly with a borrow.', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.registers[0], 255);
        assert.equal(cpu.registers[0xf], 0);
      });

      it('Subtracts correctly without a borrow.', function() {
        ncycle(cpu, 3);
        assert.equal(cpu.registers[0], 2);
        assert.equal(cpu.registers[0xf], 1);
      });
    });

    describe('0x8XYE shifts VX left by one, sets carry to most significant bit.', function() {
      // NOTE: Old implementations of the CHIP-8 used to use VY, but not anymore
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x60, 0x41, // 01000001
        0x80, 0x0e, // 10000010 0x82, no carry
        0x60, 0xc1, // 11000001
        0x80, 0x0e, // 10000010 0x82, carry
      ]));

      it('Shifts correctly when the most significant bit is 0.', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.registers[0], 0x82);
        assert.equal(cpu.registers[0xf], 0);
      });

      it('Shifts correctly when the most significant bit is 1.', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.registers[0], 0x82);
        assert.equal(cpu.registers[0xf], 1);
      });
    });

    describe('0x9XY0 skips the next instruction if VX doesn\'t equal VY.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x60, 0x01,
        0x90, 0x10,
        0x00, 0x00,
        0x61, 0x01,
        0x90, 0x10,
        0x00, 0xe0
      ]));

      // Ensure that we skip when VX doesn't equal VY
      it('Skips when VX does not equal VY', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.pc, 0x206);
      });


      // Ensure that we don't skip when VX equals VY
      it('Does not skip when VX equals VY', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.pc, 0x20a);
      });
    });

    describe('0xANNN sets I to the address NNN.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0xa1, 0x11
      ]));

      it('Sets I to the correct address.', function() {
        assert.equal(cpu.address, 0x000);
        cpu.cycle();
        assert.equal(cpu.address, 0x111);
      });
    });

    describe('0xBNNN jumps to the address NNN plus V0.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x60, 0x02,
        0xb2, 0x06,
        0x00, 0x00,
        0x00, 0x00,
        0x00, 0xe0
      ]));

      it('Jumps to the correct address.', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.pc, 0x208);
      });
    });

    describe('0xCXNN sets VX to the result of a bitwise and operation on a random number and NN.', function() {
      // First three random values for this seed:
      //   82   01010010
      //   215  11010111
      //   124  01111100
      seedrandom('spider-man', { global: true });

      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0xc0, 0xff, // 11111111 & 01010010 = 01010010 (82)
        0xc0, 0x00, // 00000000 & 11010111 = 00000000 (0)
        0xc0, 0xaa, // 10101010 & 01111100 = 00101000 (40)
      ]));

      it('Works for the first number, when and-ing with 11111111.', function() {
        cpu.cycle();
        assert.equal(cpu.registers[0], 82);
      });

      it('Works for the second number, when and-ing with 00000000.', function() {
        cpu.cycle();
        assert.equal(cpu.registers[0], 0);
      });

      it('Works for the third number, when and-ing with 10101010.', function() {
        cpu.cycle();
        assert.equal(cpu.registers[0], 40);
      });
    });

    describe('0xDXYN Draws a sprite.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x12, 0x11,
        0xff,       // 11111111
        0x00,       // 00000000
        0xaa,       // 10101010
        0x55,       // 01010101
        0xdb,       // 11011011
        0x6d,       // 01101101
        0xb6,       // 10110110
        0xee,       // 11101110
        0x77,       // 01110111
        0xf0,       // 11110000
        0x0f,       // 00001111
        0xf8,       // 11111000
        0x1f,       // 00011111
        0xfc,       // 11111100
        0x3f,       // 00111111
        0xa2, 0x02,
        0xd0, 0x01,
        0x00, 0xe0, // clear
        0xd0, 0x08,
        0x00, 0xe0, // clear
        0xd0, 0x0f,
      ]));

      it('Draws a 1-pixel-tall sprite correctly.', function() {
        ncycle(cpu, 3);
        var target = makeVideoBuffer([
          [0, 0, 1], [1, 0, 1], [2, 0, 1], [3, 0, 1], [4, 0, 1], [5, 0, 1], [6, 0, 1], [7, 0, 1]]);
        assert.deepEqual(cpu.getVideoBuffer(), target);
      });

      it('Draws an 8-pixel-tall sprite correctly.', function() {
        ncycle(cpu, 2);
        var target = makeVideoBuffer([
          [0, 0, 1], [1, 0, 1], [2, 0, 1], [3, 0, 1], [4, 0, 1], [5, 0, 1], [6, 0, 1], [7, 0, 1],   // 11111111
          [0, 1, 0], [1, 1, 0], [2, 1, 0], [3, 1, 0], [4, 1, 0], [5, 1, 0], [6, 1, 0], [7, 1, 0],   // 00000000
          [0, 2, 1], [1, 2, 0], [2, 2, 1], [3, 2, 0], [4, 2, 1], [5, 2, 0], [6, 2, 1], [7, 2, 0],   // 10101010
          [0, 3, 0], [1, 3, 1], [2, 3, 0], [3, 3, 1], [4, 3, 0], [5, 3, 1], [6, 3, 0], [7, 3, 1],   // 01010101
          [0, 4, 1], [1, 4, 1], [2, 4, 0], [3, 4, 1], [4, 4, 1], [5, 4, 0], [6, 4, 1], [7, 4, 1],   // 11011011
          [0, 5, 0], [1, 5, 1], [2, 5, 1], [3, 5, 0], [4, 5, 1], [5, 5, 1], [6, 5, 0], [7, 5, 1],   // 01101101
          [0, 6, 1], [1, 6, 0], [2, 6, 1], [3, 6, 1], [4, 6, 0], [5, 6, 1], [6, 6, 1], [7, 6, 0],   // 10110110
          [0, 7, 1], [1, 7, 1], [2, 7, 1], [3, 7, 0], [4, 7, 1], [5, 7, 1], [6, 7, 1], [7, 7, 0]    // 11101110
        ]);
        assert.deepEqual(cpu.getVideoBuffer(), target);
      });

      it('Draws an 16-pixel-tall sprite correctly.', function() {
        ncycle(cpu, 2);
        var target = makeVideoBuffer([
          [0, 0, 1], [1, 0, 1], [2, 0, 1], [3, 0, 1], [4, 0, 1], [5, 0, 1], [6, 0, 1], [7, 0, 1],           // 11111111
          [0, 1, 0], [1, 1, 0], [2, 1, 0], [3, 1, 0], [4, 1, 0], [5, 1, 0], [6, 1, 0], [7, 1, 0],           // 00000000
          [0, 2, 1], [1, 2, 0], [2, 2, 1], [3, 2, 0], [4, 2, 1], [5, 2, 0], [6, 2, 1], [7, 2, 0],           // 10101010
          [0, 3, 0], [1, 3, 1], [2, 3, 0], [3, 3, 1], [4, 3, 0], [5, 3, 1], [6, 3, 0], [7, 3, 1],           // 01010101
          [0, 4, 1], [1, 4, 1], [2, 4, 0], [3, 4, 1], [4, 4, 1], [5, 4, 0], [6, 4, 1], [7, 4, 1],           // 11011011
          [0, 5, 0], [1, 5, 1], [2, 5, 1], [3, 5, 0], [4, 5, 1], [5, 5, 1], [6, 5, 0], [7, 5, 1],           // 01101101
          [0, 6, 1], [1, 6, 0], [2, 6, 1], [3, 6, 1], [4, 6, 0], [5, 6, 1], [6, 6, 1], [7, 6, 0],           // 10110110
          [0, 7, 1], [1, 7, 1], [2, 7, 1], [3, 7, 0], [4, 7, 1], [5, 7, 1], [6, 7, 1], [7, 7, 0],           // 11101110
          [0, 8, 0], [1, 8, 1], [2, 8, 1], [3, 8, 1], [4, 8, 0], [5, 8, 1], [6, 8, 1], [7, 8, 1],           // 01110111
          [0, 9, 1], [1, 9, 1], [2, 9, 1], [3, 9, 1], [4, 9, 0], [5, 9, 0], [6, 9, 0], [7, 9, 0],           // 11110000
          [0, 10, 0], [1, 10, 0], [2, 10, 0], [3, 10, 0], [4, 10, 1], [5, 10, 1], [6, 10, 1], [7, 10, 1],   // 00001111
          [0, 11, 1], [1, 11, 1], [2, 11, 1], [3, 11, 1], [4, 11, 1], [5, 11, 0], [6, 11, 0], [7, 11, 0],   // 11111000
          [0, 12, 0], [1, 12, 0], [2, 12, 0], [3, 12, 1], [4, 12, 1], [5, 12, 1], [6, 12, 1], [7, 12, 1],   // 00011111
          [0, 13, 1], [1, 13, 1], [2, 13, 1], [3, 13, 1], [4, 13, 1], [5, 13, 1], [6, 13, 0], [7, 13, 0],   // 11111100
          [0, 14, 0], [1, 14, 0], [2, 14, 1], [3, 14, 1], [4, 14, 1], [5, 14, 1], [6, 14, 1], [7, 14, 1]    // 00111111
        ]);
        assert.deepEqual(cpu.getVideoBuffer(), target);
      });

      it('Flips pixels appropriately when sprites overlap.', function() {
        var cpu = new Cpu();
        cpu.load(new Uint8Array([
          0x12, 0x04,
          0xc0,       // 11000000
          0x80,       // 10000000
          0xa2, 0x02,
          0xd0, 0x01,
          0xa2, 0x03,
          0xd0, 0x01
        ]));

        ncycle(cpu, 3);
        var target = makeVideoBuffer([[0, 0, 1], [1, 0, 1]]);
        assert.deepEqual(cpu.getVideoBuffer(), target);

        ncycle(cpu, 2);
        target = makeVideoBuffer([[0, 0, 0], [1, 0, 1]]);
        assert.deepEqual(cpu.getVideoBuffer(), target);
      });
    });

    describe('0xEX9E Skips the next instruction if the key stored in VX is pressed.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0xe0, 0x9e,
        0xe0, 0x9e,
        0x00, 0x00,
        0x00, 0x00
      ]));

      var input = util.fillArray(new Array(16), false);
      cpu.setInputState(input);

      it('Doesn\'t skip when the key isn\'t pressed.', function() {
        cpu.cycle();
        assert.equal(cpu.pc, 0x202);
      });

      it('Does skip when the key is pressed.', function() {
        input[0] = true;
        cpu.cycle();
        assert.equal(cpu.pc, 0x206);
      });
    });

    describe('0xEXA1 Skips the next instruction if the key stored in VX isn\'t pressed.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0xe0, 0xa1,
        0xe0, 0xa1,
        0x00, 0x00,
        0x00, 0x00
      ]));

      var input = util.fillArray(new Array(16), false);
      cpu.setInputState(input);

      it('Doesn\'t skip when the key is pressed.', function() {
        input[0] = true;
        cpu.cycle();
        assert.equal(cpu.pc, 0x202);
      });

      it('Does skip when the key isn\'t pressed.', function() {
        input[0] = false;
        cpu.cycle();
        assert.equal(cpu.pc, 0x206);
      });
    });

    describe('0xFX07 Sets VX to the value of the delay timer.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0xf0, 0x07
      ]));

      it('Sets VX to delay timer.', function() {
        cpu.delayTimer = 1;
        cpu.cycle();
        assert.equal(cpu.registers[0], 1);
      });
    });

    describe('0xFX0A A key press is awaited, and then stored in VX.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0xf0, 0x0a,
        0x00, 0x00,
        0x00, 0x00
      ]));

      var input = util.fillArray(new Array(16), false);
      cpu.setInputState(input);

      it('Does not move forward until it receives a key press.', function() {
        ncycle(cpu, 100);
        assert.equal(cpu.pc, 0x202);
      });

      it('Stores the keypress in VX.', function() {
        input[1] = true;
        cpu.cycle();
        assert.equal(cpu.registers[0], 1);
      });

      it('Resumes execution after it receives a keypress.', function() {
        assert.equal(cpu.pc, 0x204);
        cpu.cycle();
        assert.equal(cpu.pc, 0x206);
      });
    });

    describe('0xFX15 Sets the delay timer to VX.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x60, 0x01,
        0xf0, 0x15
      ]));

      it('Sets the delay timer to VX.', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.delayTimer, 1);
      });
    });

    describe('0xFX18 Sets the sound timer to VX.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x60, 0x01,
        0xf0, 0x18
      ]));

      it('Sets the sound timer to VX.', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.soundTimer, 1);
      });
    });

    describe('0xFX1E Adds VX to I.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0xa3, 0x00,
        0x60, 0x02,
        0xf0, 0x1e,
        0xaf, 0xff,
        0xf0, 0x1e
      ]));

      it('Adds VX to I.', function() {
        ncycle(cpu, 3);
        assert.equal(cpu.address, 0x302);
      });

      it('Overflows correctly.', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.address, 0x001);
      });
    });

    describe('0xFX29 Sets I to the location of the sprite for the character in VX.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x60, 0x00,
        0xf0, 0x29,
        0x60, 0x01,
        0xf0, 0x29,
        0x60, 0x02,
        0xf0, 0x29,
        0x60, 0x03,
        0xf0, 0x29,
        0x60, 0x04,
        0xf0, 0x29,
        0x60, 0x05,
        0xf0, 0x29,
        0x60, 0x06,
        0xf0, 0x29,
        0x60, 0x07,
        0xf0, 0x29,
        0x60, 0x08,
        0xf0, 0x29,
        0x60, 0x09,
        0xf0, 0x29,
        0x60, 0x0a,
        0xf0, 0x29,
        0x60, 0x0b,
        0xf0, 0x29,
        0x60, 0x0c,
        0xf0, 0x29,
        0x60, 0x0d,
        0xf0, 0x29,
        0x60, 0x0e,
        0xf0, 0x29,
        0x60, 0x0f,
        0xf0, 0x29
      ]));

      it('Sets I to the address of the desired character.', function() {
        var target = 0;
        for (var i = 0; i <= 15; i++) {
          ncycle(cpu, 2);
          assert.equal(cpu.address, target);
          target += 5;
        }
      });
    });

    describe('0xFX33 Stores the binary-coded decimal representation of VX, starting at I.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0xa3, 0x00,
        0x60, 0x01, // 1
        0xf0, 0x33,
        0x60, 0x2a, // 42
        0xf0, 0x33,
        0x60, 0x7b, // 123
        0xf0, 0x33
      ]));

      it('Works with a single-digit number.', function() {
        ncycle(cpu, 3);
        assert.equal(cpu.memory[cpu.address], 0);
        assert.equal(cpu.memory[cpu.address + 1], 0);
        assert.equal(cpu.memory[cpu.address + 2], 1);
      });

      it('Works with a double-digit number.', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.memory[cpu.address], 0);
        assert.equal(cpu.memory[cpu.address + 1], 4);
        assert.equal(cpu.memory[cpu.address + 2], 2);
      });

      it('Works with a triple-digit number.', function() {
        ncycle(cpu, 2);
        assert.equal(cpu.memory[cpu.address], 1);
        assert.equal(cpu.memory[cpu.address + 1], 2);
        assert.equal(cpu.memory[cpu.address + 2], 3);
      });
    });

    describe('0xFX55 Stores V0 to VX (including VX) in memory starting at address I.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0xa3, 0x00,
        0x60, 0x01,
        0x61, 0x02,
        0x62, 0x03,
        0x63, 0x04,
        0x64, 0x05,
        0x65, 0x06,
        0x66, 0x07,
        0x67, 0x08,
        0x68, 0x09,
        0x69, 0x0a,
        0x6a, 0x0b,
        0x6b, 0x0c,
        0x6c, 0x0d,
        0x6d, 0x0e,
        0x6e, 0x0f,
        0x6f, 0x10,
        0xf0, 0x55,
        0xf7, 0x55,
        0xff, 0x55
      ]));

      it('Works with a single register.', function() {
        ncycle(cpu, 18);
        var target = new Uint8Array([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        assert.deepEqual(cpu.memory.slice(cpu.address, cpu.address + 16), target);
      });

      it('Works with eight registers.', function() {
        cpu.cycle();
        var target = new Uint8Array([1, 2, 3, 4, 5, 6, 7, 8, 0, 0, 0, 0, 0, 0, 0, 0]);
        assert.deepEqual(cpu.memory.slice(cpu.address, cpu.address + 16), target);
      });

      it('Works with all registers.', function() {
        cpu.cycle();
        var target = new Uint8Array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]);
        assert.deepEqual(cpu.memory.slice(cpu.address, cpu.address + 16), target);
      });
    });

    describe('0xFX65 Fills V0 to VX (including VX) with values from memory starting at address I.', function() {
      var cpu = new Cpu();
      cpu.load(new Uint8Array([
        0x12, 0x12,
        0x01,
        0x02,
        0x03,
        0x04,
        0x05,
        0x06,
        0x07,
        0x08,
        0x09,
        0x0a,
        0x0b,
        0x0c,
        0x0d,
        0x0e,
        0x0f,
        0x10,
        0xa2, 0x02,
        0xf0, 0x65,
        0xf7, 0x65,
        0xff, 0x65
      ]));

      it('Works with a single register.', function() {
        ncycle(cpu, 3);
        var target = new Uint8Array([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        assert.deepEqual(cpu.registers, target);
      });

      it('Works with eight registers.', function() {
        cpu.cycle();
        var target = new Uint8Array([1, 2, 3, 4, 5, 6, 7, 8, 0, 0, 0, 0, 0, 0, 0, 0]);
        assert.deepEqual(cpu.registers, target);
      });

      it('Works with all registers.', function() {
        cpu.cycle();
        var target = new Uint8Array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]);
        assert.deepEqual(cpu.registers, target);
      });
    });
  });
});

function ncycle(cpu, n) {
  for (var i = 0; i < n; i++) {
    cpu.cycle();
  }
}

function makeVideoBuffer(pixels) {
  var WIDTH = 64;
  var HEIGHT = 32;

  var buffer = util.fillArray(new Array(WIDTH * HEIGHT), false);
  for (var i = 0; i < pixels.length; i++) {
    var pixel = pixels[i];
    buffer[pixel[1] * WIDTH + pixel[0]] = pixel[2] ? true : false;
  }
  return buffer;
}
