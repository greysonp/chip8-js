var SimpleSpeaker = function() {
  this.oscillator = null;
  this.context = new AudioContext;
  this.playing = false;
}

SimpleSpeaker.prototype.start = function() {
  if (!this.playing) {
    this.oscillator = this.context.createOscillator();
    this.oscillator.frequency.value = 1000;
    this.oscillator.connect(this.context.destination);
    this.oscillator.start(0);
    this.playing = true;
  }
}

SimpleSpeaker.prototype.stop = function() {
  if (this.oscillator) {
    this.oscillator.stop();
    this.playing = false;
  }
}

module.exports = SimpleSpeaker;
