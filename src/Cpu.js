var util = require('./util/Util');

var FONT_SET = new Uint8Array([
  0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
  0x20, 0x60, 0x20, 0x20, 0x70, // 1
  0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
  0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
  0x90, 0x90, 0xF0, 0x10, 0x10, // 4
  0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
  0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
  0xF0, 0x10, 0x20, 0x40, 0x40, // 7
  0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
  0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
  0xF0, 0x90, 0xF0, 0x90, 0x90, // A
  0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
  0xF0, 0x80, 0x80, 0x80, 0xF0, // C
  0xE0, 0x90, 0x90, 0x90, 0xE0, // D
  0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
  0xF0, 0x80, 0xF0, 0x80, 0x80  // F
]);

var PROGRAM_START = 0x200;
var CARRY = 0xF;
var WIDTH = 64;
var HEIGHT = 32;

var Cpu = function(randomNumberGenerator) {
  this.rng = randomNumberGenerator || require('./util/RandomByte');
  this.reset();
}

Cpu.prototype.reset = function() {
  // 12-bit program counter
  this.pc = PROGRAM_START;

  // 4096 8-bit (1 byte) memory locations
  this.memory = new Uint8Array(4096);

  // 16 8-bit (1 byte) registers
  this.registers = new Uint8Array(16);

  // 16-bit (2 byte) address register
  this.address = 0;

  // 16 4-bit stack entries
  this.stack = new Array(16);

  // The pointer to tell us where we are in the stack
  this.sp = 0;

  // A timer that counts down at 60hz when non-zero
  this.delayTimer = 0;

  // Plays a song when non-zero. Counts down at 60hz.
  this.soundTimer = 0;

  // Whether or not the system is currently stalled waiting for input
  this.waitingForInput = -1;

  // A chunk of memory where each index is a boolean representing whether a pixel is on or off
  this.video = util.fillArray(new Array(WIDTH * HEIGHT), false);

  // A list of booleans that indicates if a key is pressed or not
  this.input = util.fillArray(new Array(16), false);

  this._loadFontSet();
}

Cpu.prototype.load = function(program) {
  // Read program into memory
  for (var i = 0; i < program.length; i++) {
    this.memory[PROGRAM_START + i] = program[i];
  }
}

Cpu.prototype.getVideoBuffer = function() {
  return this.video;
}

Cpu.prototype.setInputState = function(state) {
  this.input = state;
}

Cpu.prototype.updateTimers = function() {
  this.delayTimer = Math.max(0, this.delayTimer - 1);
  this.soundTimer = Math.max(0, this.soundTimer - 1);
}

Cpu.prototype.cycle = function() {
  if (this.waitingForInput >= 0) {
    var pressed = this._pressed();
    if (pressed.length > 0) {
      this.registers[this.waitingForInput] = pressed[0];
      this.waitingForInput = -1;
    } else {
      return;
    }
  }

  // Opcodes are always 2 bytes, and each memory address holds 1 byte, so we can just combine the next two memory
  // addresses into one value to make it easier to read
  var opcode = this.memory[this.pc] << 8 | this.memory[this.pc + 1];
  this.pc += 2;

  // We can calculate these values for every opcode for the sake of convenience
  // We're using the terminology specified on the Wikipedia article.
  // http://en.wikipedia.org/wiki/CHIP-8#Opcode_table
  var vx  = (opcode & 0x0f00) >> 8;
  var vy  = (opcode & 0x00f0) >> 4;
  var nnn = opcode & 0x0fff;
  var nn  = opcode & 0x00ff;
  var n   = opcode & 0x000f;

  // The opcodes generally use the leading 4 bits as an identifier, then use the remain 12 bits as data
  switch (opcode & 0xf000) {
    case 0x0000:
        switch(opcode) {
          case 0x00e0:
            // Clears the screen
            for (var i = 0; i < this.video.length; i++) {
              this.video[i] = false;
            }
            break;
          case 0x00ee:
            // Return from a subroutine
            this.sp--;
            this.pc = this.stack[this.sp];
        }
        break;

    case 0x1000:
      // Jump to address NNN
      this.pc = nnn;
      break;

    case 0x2000:
      // Call subroutine at address NNN
      // Because we've already incremented the PC, we can leave it alone, and when we return from the subroutine, it'll
      // just go to the next instruction as desired.
      this.stack[this.sp] = this.pc;
      this.sp++;
      this.pc = nnn;
      break;

    case 0x3000:
      // Skips the next instruction if VX equals NN
      if (this.registers[vx] === nn) {
        this.pc += 2;
      }
      break;

    case 0x4000:
      // Skips the next instruction if VX doesn't equal NN
      if (this.registers[vx] !== nn) {
        this.pc += 2;
      }
      break;

    case 0x5000:
      // Skips the next instruction if VX equals VY
      if (this.registers[vx] === this.registers[vy]) {
        this.pc += 2;
      }
      break;

    case 0x6000:
      // Sets VX to NN
      this.registers[vx] = nn;
      break;

    case 0x7000:
      // Adds NN to VX
      this.registers[vx] += nn;
      break;

    case 0x8000:
      switch (opcode & 0x000f) {
        case 0x0000:
          // Sets VX to the value of VY
          this.registers[vx] = this.registers[vy];
          break;

        case 0x0001:
          // Sets VX to VX | VY
          this.registers[vx] = this.registers[vx] | this.registers[vy];
          break;

        case 0x0002:
          // Sets VX to VX & VY
          this.registers[vx] = this.registers[vx] & this.registers[vy];
          break;

        case 0x0003:
          // Sets VX to VX ^ VY
          this.registers[vx] = this.registers[vx] ^ this.registers[vy];
          break;

        case 0x0004:
          // Adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there isn't.
          this.registers[CARRY] = this.registers[vx] + this.registers[vy] > 255 ? 1 : 0;
          this.registers[vx] += this.registers[vy];
          break;

        case 0x0005:
          // VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there isn't
          this.registers[CARRY] = this.registers[vx] - this.registers[vy] < 0 ? 0 : 1;
          this.registers[vx] -= this.registers[vy];
          break;

        case 0x0006:
          // Shifts VX right by one. VF is set to the value of the least significant bit of VX before the shift.
          this.registers[CARRY] = this.registers[vx] & 0x1;
          this.registers[vx] = this.registers[vx] >> 1;
          break;

        case 0x0007:
          // Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there isn't.
          this.registers[CARRY] = this.registers[vy] - this.registers[vx] < 0 ? 0 : 1;
          this.registers[vx] = this.registers[vy] - this.registers[vx];
          break;

        case 0x000e:
          // Shifts VX left by one. VF is set to the value of the most significant bit of VX before the shift.
          this.registers[CARRY] = (this.registers[vx] & 0x80) >> 7;
          this.registers[vx] = this.registers[vx] << 1;
          break;
      }
      break;

    case 0x9000:
      // Skips the next instruction if VX doesn't equal VY.
      if (this.registers[vx] !== this.registers[vy]) {
        this.pc += 2;
      }
      break;

    case 0xa000:
      // Sets I to the address NNN.
      this.address = nnn;
      break;

    case 0xb000:
      // Jumps to the address NNN plus V0.
      this.pc = nnn + this.registers[0];
      break;

    case 0xc000:
      // Sets VX to the result of a bitwise and operation on a random number and NN.
      this.registers[vx] = this.rng.next() & nn;
      break;

    case 0xd000:
      // Draws a sprite at coordinate (VX, VY) that has a width of 8 pixels and a height of N pixels. Each row of 8
      // pixels is read as bit-coded starting from memory location I; I value doesn’t change after the execution of
      // this instruction. VF is set to 1 if any screen pixels are flipped from set to unset when the sprite is drawn,
      // and to 0 if that doesn’t happen.
      this.registers[CARRY] = this._draw(
        this.registers[vx],
        this.registers[vy],
        this.memory.slice(this.address, this.address + n));
      break;

    case 0xe000:
      switch (opcode & 0x00ff) {
        case 0x009e:
          // Skips the next instruction if the key stored in VX is pressed.
          if (this._pressed().indexOf(this.registers[vx]) >= 0) {
            this.pc += 2;
          }
          break;
        case 0x00a1:
          // Skips the next instruction if the key stored in VX isn't pressed.
          if (this._pressed().indexOf(this.registers[vx]) < 0) {
            this.pc += 2;
          }
          break;
      }
      break;

    case 0xf000:
      switch (opcode & 0x00ff) {
        case 0x0007:
          // Sets VX to the value of the delay timer.
          this.registers[vx] = this.delayTimer;
          break;

        case 0x000a:
          // A key press is awaited, and then stored in VX.
          this.waitingForInput = vx;
          break;

        case 0x0015:
          // Sets the delay timer to VX.
          this.delayTimer = this.registers[vx];
          break;

        case 0x0018:
          // Sets the sound timer to VX.
          this.soundTimer = this.registers[vx];
          break;

        case 0x001e:
          // Adds VX to I.
          this.address = uint12(this.address + this.registers[vx]);
          break;

        case 0x0029:
          // Sets I to the location of the sprite for the character in VX. Characters 0-F (in hexadecimal) are
          // represented by a 4x5 font.
          this.address = this.registers[vx] * 5;
          break;

        case 0x0033:
          // Stores the binary-coded decimal representation of VX, with the most significant of three digits at the
          // address in I, the middle digit at I plus 1, and the least significant digit at I plus 2. (In other words,
          // take the decimal representation of VX, place the hundreds digit in memory at location in I, the tens
          // digit at location I+1, and the ones digit at location I+2.)
          var start = this.registers[vx];
          this.memory[this.address] = parseInt(start / 100);
          start = start % 100
          this.memory[this.address + 1] = parseInt(start / 10);
          start = start % 10;
          this.memory[this.address + 2] = start;
          break;

        case 0x0055:
          // Stores V0 to VX (including VX) in memory starting at address I.
          for (var i = 0; i <= vx; i++) {
            this.memory[this.address + i] = this.registers[i];
          }
          break;

        case 0x0065:
          // Fills V0 to VX (including VX) with values from memory starting at address I.
          for (var i = 0; i <= vx; i++) {
            this.registers[i] = this.memory[this.address + i];
          }
          break;
      }

      break;
  }

  this.pc = uint12(this.pc);
}

Cpu.prototype._loadFontSet = function() {
  for (var i = 0; i < FONT_SET.length; i++) {
    this.memory[i] = FONT_SET[i];
  }
}

Cpu.prototype._draw = function(x, y, sprite) {
  // Look through each byte and set each pixel in the video buffer
  var unset = false;
  for (var i = 0; i < sprite.length; i++) {
    var val = sprite[i];
    unset |= this._setPixel(uint8(x + 0), uint8(y + i), (val & 0x80) > 0);
    unset |= this._setPixel(uint8(x + 1), uint8(y + i), (val & 0x40) > 0);
    unset |= this._setPixel(uint8(x + 2), uint8(y + i), (val & 0x20) > 0);
    unset |= this._setPixel(uint8(x + 3), uint8(y + i), (val & 0x10) > 0);
    unset |= this._setPixel(uint8(x + 4), uint8(y + i), (val & 0x08) > 0);
    unset |= this._setPixel(uint8(x + 5), uint8(y + i), (val & 0x04) > 0);
    unset |= this._setPixel(uint8(x + 6), uint8(y + i), (val & 0x02) > 0);
    unset |= this._setPixel(uint8(x + 7), uint8(y + i), (val & 0x01) > 0);
  }
  return unset ? 1 : 0;
}

Cpu.prototype._setPixel = function(x, y, state) {
  if (x >= WIDTH || x < 0 || y >= HEIGHT || y < 0) {
    return;
  }
  var index = y * WIDTH + x;
  var original = this.video[index];
  this.video[index] = original ^ state ? true : false;

  // Return whether or not we flipped the pixel from on to off
  return original && !this.video[index];
}

Cpu.prototype._pressed = function() {
  var pressed = [];
  for (var i = 0; i < this.input.length; i++) {
    if (this.input[i]) {
      pressed.push(i);
    }
  }
  return pressed;
}

function uint8(val) {
  return val % 256;
}

function uint12(val) {
  return val % 4096;
}

module.exports = Cpu;
