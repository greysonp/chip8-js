var diff = require('deep-diff').diff;

var DataStore = function() {
  this.sprites = {};
  this.activeSprite = null;
  this.listeners = {};
  this.ACTIVE_SPRITE_CHANGED = 'active_sprite_changed';
  this.SPRITES_CHANGED = 'sprites_changed';
  this.ACTION_SPRITE_RENAMED = 'action_sprite_renamed'
}

DataStore.prototype.setActiveSprite = function(name) {
  if (name === this.activeSprite || (name && !this.sprites[name])) {
    return;
  }
  this.activeSprite = name;
  this.triggerEvent(this.ACTIVE_SPRITE_CHANGED);
}

DataStore.prototype.getActiveSprite = function() {
  return this.activeSprite;
}

DataStore.prototype.putSprite = function(name, data) {
  // Empty data will remove the sprite
  if (this.sprites[name] && data.trim().length === 0) {
    delete this.sprites[name];
    this.triggerEvent(this.SPRITES_CHANGED);
  } else if (!this.sprites[name] || (this.sprites[name] && diff(this.sprites[name], data))) {
    this.sprites[name] = data;
    this.triggerEvent(this.SPRITES_CHANGED);
  }
}

DataStore.prototype.renameSprite = function(oldName, newName) {
  if (newName === oldName || !this.sprites[oldName]) {
    return;
  }

  this.sprites[newName] = this.sprites[oldName];
  delete this.sprites[oldName];
  
  // Keep active sprite in sync
  if (oldName === this.activeSprite) {
    this.activeSprite = newName;
  }
  
  this.triggerEvent(this.SPRITES_CHANGED, {
    action: this.ACTION_SPRITE_RENAMED,
    oldName: oldName,
    newName: newName
  });
}

DataStore.prototype.removeSprite = function(name) {
  delete this.sprites[name];
  if (name == this.activeSprite) {
    this.setActiveSprite(null);
  }
  this.triggerEvent(this.SPRITES_CHANGED);
}

DataStore.prototype.getSprite = function(name) {
  return this.sprites[name];
}

DataStore.prototype.setSprites = function(sprites) {
  var spriteDiff = diff(this.sprites, sprites);
  if (spriteDiff) {
    // Handle special 'rename' case (we have 1 deletion and 1 creation with the same data)
    if (spriteDiff.length == 2 && spriteDiff[0].kind === 'D' && spriteDiff[1].kind === 'N' && spriteDiff[0].lhs === spriteDiff[1].rhs) {
      this.renameSprite(spriteDiff[0].path[0], spriteDiff[1].path[0]);
      return;
    }

    // If the active sprite has been removed, get rid of it
    if (this.activeSprite && !sprites[this.activeSprite]) {
      this.setActiveSprite(null);
    }

    // If not a rename, send out a generic change
    this.sprites = sprites;
    this.triggerEvent(this.SPRITES_CHANGED);
  }
}

DataStore.prototype.getSprites = function() {
  return this.sprites;
}

DataStore.prototype.addEventListener = function(event, callback) {
  this.listeners[event] = this.listeners[event] || [];
  this.listeners[event].push(callback);
}

DataStore.prototype.removeEventListener = function(event, callback) {
  if (!this.listeners[event]) {
    return;
  }

  this.listeners[event] = this.listeners[event].filter(function(e) {
    return e !== callback;
  });
}

DataStore.prototype.triggerEvent = function(event, data) {
  if (!this.listeners[event]) {
    return;
  }

  this.listeners[event].forEach(function(callback) {
    callback(data);
  })
}

module.exports = DataStore;