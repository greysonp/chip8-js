var HexCompiler = function() {

}

HexCompiler.prototype.compile = function(text) {
  var tokens = text.split(/\s/);
  var bytes = tokens.map(function(token) {
    return parseInt(token, 16);
  });
  return new Uint8Array(bytes);
}

module.exports = HexCompiler;
