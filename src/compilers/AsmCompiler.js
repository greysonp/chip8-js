var Util = require('../util/Util');

var AsmCompiler = function() {
}

AsmCompiler.prototype.compile = function(text) {
  var tokenResult = textToTokens(text);
  return tokensToBytes(tokenResult.tokens, tokenResult.symbols);
}

function textToTokens(text) {
  var pc = 0x200;
  var symbols = {};
  var instructions = [];

  var lines = text.split(/\n/);

  lines.forEach(function(line, lineNumber) {
    // We want our line numbers to start at 1, not 0
    lineNumber += 1;

    // Get the tokens for the line and make them lowercase
    var tokens = line.trim().split(/\s+/);
    tokens = tokens.map(function(token) {
      return token.toLowerCase();
    });
    var instr = tokens[0];

    // Skip blank lines
    if (instr.length === 0) {
      return;
    }

    // Skip comments
    if (instr.startsWith('//')) {
      return;
    }

    // Parse labels
    if (instr.startsWith(':')) {
      var label = instr.substring(1);
      if (tokens.length > 1) {
        printError('Unexpected tokens after label declaration', lineNumber);
      } else if (!isValidLabel(label)) {
        printError('Invalid label \'' + label, lineNumber);
      } else if (symbols[instr] != null) {
        printError('Duplicate declaration of label \'' + label + '\'', lineNumber);
      } else {
        symbols[label] = pc;
      }
      return;
    }

    // Parse raw data (like sprite data)
    var data = DATA(instr);
    if (data !== null) {
      tokens.forEach(function(token) {
        data = DATA(token);
        if (data !== null) {
          instructions.push(INSTR('data', [ data ], lineNumber));
          pc += 1;
        } else {
          printError('Invalid data');
        }
      });
      return;
    }

    // If we made it this far, we have a regular instruction
    var args = tokens.slice(1);
    switch (instr) {
      case 'cls':
        if (args.length > 0) {
          printError('Expected no arguments after CLS, found \'' + args.join(' ') + '\'', lineNumber);
        }
        instructions.push(INSTR('cls', [], lineNumber));
        break;

      case 'ret':
        if (args.length > 0) {
          printError('Unexpected no aguments after CLS, found \'' + args.join(' ') + '\'', lineNumber);
        }
        instructions.push(INSTR('ret', [], lineNumber));
        break;

      case 'jp':
        // addr
        // V0, addr
        if (args.length == 1) {
          var address = ADDR(args[0])
          if (address !== null) {
            instructions.push(INSTR('jp', [ address ], lineNumber));
          } else {
            printError('Invalid address');
          }
        } else if (args.length == 2) {
          var v0 = VO(args[0]);
          var address = ADDR(args[1]);
          if (v0 !== null && address !== null) {
            instructions.push(INSTR('jp', [ v0, address ], lineNumber));
          } else {
            printError('Invalid register or address');
          }
        } else {
          printError('Expected (address) or (V0, address), found \'' + args.join(' ') + '\'', lineNumber);
        }
        break;

      case 'call':
        // addr
        if (args.length == 1) {
          var address = ADDR(args[0]);
          if (address !== null) {
            instructions.push(INSTR('call', [ address ], lineNumber));
          } else {
            printError('Invalid address', lineNumber);
          }
        } else {
          printError('Expected (address), found \'' + args.join(' ') + '\'', lineNumber);
        }
        break;

      case 'se':
        // Vx, byte
        // Vx, Vy
        if (args.length == 2) {
          var vx = REG(args[0]);
          if (vx !== null) {
            var vy = REG(args[1]);
            var byte = BYTE(args[1]);
            if (vy != null) {
              instructions.push(INSTR('se', [ vx, vy ], lineNumber));
            } else if (byte != null) {
              instructions.push(INSTR('se', [ vx, byte ], lineNumber));
            } else {
              printError('Invalid second argument');
            }
          } else {
            printError('Invalid Vx', lineNumber);
          }
        } else {
          printError('Expected (Vx, byte) or (Vx, Vy), found \'' + args.join(' ') + '\'', lineNumber);
        }
        break;

      case 'sne':
      // Vx, byte
      // Vx, Vy
      if (args.length == 2) {
        var vx = REG(args[0]);
        if (vx !== null) {
          var vy = REG(args[1]);
          var byte = BYTE(args[1]);
          if (vy != null) {
            instructions.push(INSTR('sne', [ vx, vy ], lineNumber));
          } else if (byte != null) {
            instructions.push(INSTR('sne', [ vx, byte ], lineNumber));
          } else {
            printError('Invalid second argument');
          }
        } else {
          printError('Invalid Vx', lineNumber);
        }
      } else {
        printError('Expected (Vx, byte) or (Vx, Vy), found \'' + args.join(' ') + '\'', lineNumber);
      }
      break;

      case 'ld':
        // Vx, byte
        // Vx, Vy
        // I, addr
        // Vx, DT
        // Vx, k
        // DT, Vx
        // ST, Vx
        // F, Vx
        // B, Vx
        // [I], Vx
        // Vx, [I]
        if (args.length == 2) {
          var vx = REG(args[0]);
          var i = I(args[0]);
          var dt = DT(args[0]);
          var st = ST(args[0]);
          var font = FONT(args[0]);
          var bcd = BCD(args[0]);
          var irange = IRANGE(args[0]);
          if (vx !== null) {
            dt = DT(args[1]);
            irange = IRANGE(args[1]);
            var key = KEY(args[1]);
            var byte = BYTE(args[1]);
            var vy = BYTE(args[1]);
            if (dt !== null) {
              instructions.push(INSTR('ld', [ vx, dt ], lineNumber));
            } else if (key !== null) {
              instructions.push(INSTR('ld', [ vx, key ], lineNumber));
            } else if (irange !== null) {
              instructions.push(INSTR('ld', [ vx, irange ], lineNumber));
            } else if (byte !== null) {
              instructions.push(INSTR('ld', [ vx, byte ], lineNumber));
            } else if (vy !== null) {
              instructions.push(INSTR('ld', [ vx, vy ], lineNumber));
            } else {
              printError('Invalid second argument', lineNumber);
            }
          } else if (i !== null) {
            var address = ADDR(args[1]);
            if (address !== null) {
              instructions.push(INSTR('ld', [ i, address ], lineNumber));
            } else {
              printError('Invalid second argument', lineNumber);
            }
          } else if (dt !== null) {
            var vy = REG(args[1]);
            if (vy !== null) {
              instructions.push(INSTR('ld', [ dt, vy , lineNumber]))
            } else {
              printError('Invalid second argument', lineNumber);
            }
          } else if (st !== null) {
            var vy = REG(args[1]);
            if (vy !== null) {
              instructions.push(INSTR('ld', [ st, vy , lineNumber]))
            } else {
              printError('Invalid second argument', lineNumber);
            }
          } else if (font !== null) {
            var vy = REG(args[1]);
            if (vy !== null) {
              instructions.push(INSTR('ld', [ font, vy , lineNumber]))
            } else {
              printError('Invalid second argument', lineNumber);
            }
          } else if (bcd !== null) {
            var vy = REG(args[1]);
            if (vy !== null) {
              instructions.push(INSTR('ld', [ bcd, vy , lineNumber]))
            } else {
              printError('Invalid second argument', lineNumber);
            }
          } else if (irange !== null) {
            var vy = REG(args[1]);
            if (vy !== null) {
              instructions.push(INSTR('ld', [ irange, vy , lineNumber]))
            } else {
              printError('Invalid second argument', lineNumber);
            }
          }
        } else {
          printError('Unexpected number of arguments', lineNumber);
        }
        break;

      case 'or':
        // Vx, Vy
        if (args.length == 2) {
          var vx = REG(args[0]);
          var vy = REG(args[1]);
          if (vx !== null && vy !== null) {
            instructions.push(INSTR('or', [ vx, vy ], lineNumber));
          } else {
            printError('One or more invalid registers', lineNumber);
          }
        } else {
          printError('Expected (Vx, Vy), found \'' + args.join(' ') + '\'', lineNumber);
        }
        break;

      case 'and':
        // Vx, Vy
        if (args.length == 2) {
          var vx = REG(args[0]);
          var vy = REG(args[1]);
          if (vx !== null && vy !== null) {
            instructions.push(INSTR('and', [ vx, vy ], lineNumber));
          } else {
            printError('One or more invalid registers', lineNumber);
          }
        } else {
          printError('Expected (Vx, Vy), found \'' + args.join(' ') + '\'', lineNumber);
        }
        break;

      case 'xor':
        // Vx, Vy
        if (args.length == 2) {
          var vx = REG(args[0]);
          var vy = REG(args[1]);
          if (vx !== null && vy !== null) {
            instructions.push(INSTR('xor', [ vx, vy ], lineNumber));
          } else {
            printError('One or more invalid registers', lineNumber);
          }
        } else {
          printError('Expected (Vx, Vy), found \'' + args.join(' ') + '\'', lineNumber);
        }
        break;

      case 'add':
        // Vx, Vy
        // Vx, byte
        if (args.length == 2) {
          var vx = REG(args[0]);
          var vy = REG(args[1]);
          if (vx !== null) {
            var vy = REG(args[1]);
            var byte = BYTE(args[1]);
            if (vy !== null) {
              instructions.push(INSTR('add', [ vx, vy ], lineNumber));
            } else if (byte !== null) {
              instructions.push(INSTR('add', [ vx, byte ], lineNumber));
            } else {
              printError('Invalid second argument', lineNumber);
            }
          } else {
            printError('Invalid first argument', lineNumber);
          }
        } else {
          printError('Expected (Vx, Vy), found \'' + args.join(' ') + '\'', lineNumber);
        }
        break;

      case 'sub':
        // Vx, Vy
        if (args.length == 2) {
          var vx = REG(args[0]);
          var vy = REG(args[1]);
          if (vx !== null && vy !== null) {
            instructions.push(INSTR('sub', [ vx, vy ], lineNumber));
          } else {
            printError('One or more invalid registers', lineNumber);
          }
        } else {
          printError('Expected (Vx, Vy), found \'' + args.join(' ') + '\'', lineNumber);
        }
        break;

      case 'shr':
        // Vx
        if (tokens.length == 2) {
          var vx = REG(args[0]);
          if (vx !== null) {
            instructions.push(INSTR('shr', [ vx ], lineNumber));
          } else {
            printError('Invalid register', lineNumber);
          }
        } else {
          printError('Expected (Vx), found \'' + args.join(' ') + '\'', lineNumber);
        }
        break;

      case 'subn':
        // Vx, Vy
        if (args.length == 2) {
          var vx = REG(args[0]);
          var vy = REG(args[1]);
          if (vx !== null && vy !== null) {
            instructions.push(INSTR('subn', [ vx, vy ], lineNumber));
          } else {
            printError('Invalid register', lineNumber);
          }
        } else {
          printError('Expected (Vx, Vy), found \'' + args.join(' ') + '\'', lineNumber);
        }
        break;

      case 'shl':
        // Vx
        if (tokens.length == 2) {
          var vx = REG(args[0]);
          if (vx !== null) {
            instructions.push(INSTR('shl', [ vx ], lineNumber));
          } else {
            printError('Invalid register', lineNumber);
          }
        } else {
          printError('Expected (Vx), found \'' + args.join(' ') + '\'', lineNumber);
        }
        break;

      case 'rnd':
        // Vx, byte
        if (args.length == 2) {
          var vx = REG(args[0]);
          var byte = BYTE(args[1]);
          if (vx !== null && byte !== null) {
            instructions.push(INSTR('rnd', [ vx, byte ], lineNumber));
          } else {
            printError('Invalid arguments', lineNumber);
          }
        } else {
          printError('Expected (Vx, byte), found \'' + args.join(' ') + '\'', lineNumber);
        }
        break;

      case 'drw':
        // Vx, Vy, nibble
        if (args.length == 3) {
          var vx = REG(args[0]);
          var vy = REG(args[1]);
          var nibble = NIBBLE(args[2]);
          if (vx !== null && vy !== null && nibble !== null) {
            instructions.push(INSTR('drw', [ vx, vy, nibble ], lineNumber));
          } else {
            printError('Invalid arguments', lineNumber);
          }
        } else {
          printError('Expected (Vx, Vy, nibble), found \'' + args.join(' ') + '\'', lineNumber);
        }
        break;

      case 'skp':
        // Vx
        if (args.length == 1) {
          var vx = REG(args[0]);
          if (vx !== null) {
            instructions.push(INSTR('skp', [ vx ], lineNumber));
          } else {
            printError('Expected (Vx), found \'' + args.join(' ') + '\'', lineNumber);
          }
        }
        break;

      case 'sknp':
        // Vx
        if (args.length == 1) {
          var vx = REG(args[0]);
          if (vx !== null) {
            instructions.push(INSTR('sknp', [ vx ], lineNumber));
          } else {
            printError('Expected (Vx), found \'' + args.join(' ') + '\'', lineNumber);
          }
        }
        break;

      default:
        printError('Unknown instruction \'' + instr + '\'', lineNumber);
    }

    // Increment the program counter
    pc += 2;
  });

  return {
    tokens: instructions,
    symbols: symbols
  };
}

function tokensToBytes(instructions, symbols) {
  var out = [];

  instructions.forEach(function(instruction) {
    // Update addresses from symbol map if necessary
    instruction.args.forEach(function(arg) {
        if (arg.type == 'address') {
          var addr = getAddress(arg.val, symbols);
          if (!addr) {
            printError('Could not find symbol \'' + arg.val + '\'', instruction.lineNumber);
          } else {
            arg.val = addr;
          }
        }
    });

    var args = instruction.args;

    switch (instruction.opcode) {
      case 'cls':
        out.push(0x00);
        out.push(0xe0);
        break;

      case 'ret':
        out.push(0x00);
        out.push(0xee);
        break;

      case 'jp':
        // addr
        // V0, addr
        if (args[0].type == 'address') {
          pushAddress(0x10, args[0].val, out);
        } else if (args[0].type == 'v0') {
          pushAddress(0xb0, args[1].val, out);
        }
        break;

      case 'call':
        // addr
        pushAddress(0x20, args[0].val, out);
        break;

      case 'se':
        // Vx, byte
        // Vx, Vy
        if (args[1].type == 'byte') {
          out.push(0x30 | args[0].val);
          out.push(args[1].val);
        } else if (args[1].type == 'register'){
          out.push(0x50 | args[0].val);
          out.push(args[1].val << 4);
        }
        break;

      case 'sne':
        // Vx, byte
        // Vx, Vy
        if (args[1].type == 'byte') {
          out.push(0x40 | args[0].val);
          out.push(args[1].val);
        } else if (args[1].type == 'register'){
          out.push(0x90 | args[0].val);
          out.push(args[1].val << 4);
        }
        break;

      case 'ld':
        // Vx, byte
        // Vx, Vy
        // I, addr
        // Vx, DT
        // Vx, k
        // DT, Vx
        // ST, Vx
        // F, Vx
        // B, Vx
        // [I], Vx
        // Vx, [I]
        if (args[0].type == 'i') {
          pushAddress(0xa0, args[1].val, out);
        } else if (args[0].type == 'register') {
          if (args[1].type == 'dt') {
            out.push(0xf0 | args[0].val);
            out.push(0x07);
          } else if (args[1].type == 'key') {
            out.push(0xf0 | args[0].val);
            out.push(0x0a);
          } else if (args[1].type == 'irange') {
            out.push(0xf0 | args[0].val);
            out.push(0x65);
          } else if (args[1].type == 'byte') {
            out.push(0x60 | args[0].val);
            out.push(args[1].val);
          } else if (args[1].type == 'vy') {
            out.push(0x80 | args[0].val);
            out.push(args[1].val << 4);
          }
        } else if (args[0].type == 'dt') {
          out.push(0xf0 | args[1].val);
          out.push(0x15);
        } else if (args[0].type == 'st') {
          out.push(0xf0 | args[1].val);
          out.push(0x18);
        } else if (args[0].type == 'font') {
          out.push(0xf0 | args[1].val);
          out.push(0x29);
        } else if (args[0].type == 'bcd') {
          out.push(0xf0 | args[1].val);
          out.push(0x33);
        } else if (args[0].type == 'irange') {
          out.push(0xf0 | args[1].val);
          out.push(0x55);
        }
        break;

      case 'or':
        // Vx, Vy
        out.push(0x80 | args[0].val);
        out.push((args[1].val << 4) | 0x01);
        break;

      case 'and':
        // Vx, Vy
        out.push(0x80 | args[0].val);
        out.push((args[1].val << 4) | 0x02);
        break;

      case 'xor':
        // Vx, Vy
        out.push(0x80 | args[0].val);
        out.push((args[1].val << 4) | 0x03);
        break;

      case 'add':
        // Vx, Vy
        // Vx, Byte
        if (args[1].type == 'register') {
          out.push(0x80 | args[0].val);
          out.push((args[1].val << 4) | 0x04);
        } else if (args[1].type == 'byte') {
          out.push(0x70 | args[0].val);
          out.push(args[1].val);
        }

        break;

      case 'sub':
        // Vx, Vy
        out.push(0x80 | args[0].val);
        out.push((args[1].val << 4) | 0x05);
        break;

      case 'shr':
        // Vx
        out.push(0x80 | args[0].val);
        out.push(0x06);
        break;

      case 'subn':
        // Vx, Vy
        out.push(0x80 | args[0].val);
        out.push((args[1].val << 4) | 0x07);
        break;

      case 'shl':
        // Vx
        out.push(0x80 | args[0].val);
        out.push(0x0e);
        break;

      case 'rnd':
        // Vx, byte
        out.push(0xc0 | args[0].val);
        out.push(args[1].val);
        break;

      case 'drw':
        // Vx, Vy, nibble
        out.push(0xd0 | args[0].val);
        out.push((args[1].val << 4) | args[2].val);
        break;

      case 'skp':
        // Vx
        out.push(0xe0 | args[0].val);
        out.push(0x9e);
        break;

      case 'sknp':
        // Vx
        out.push(0xe0 | args[0].val);
        out.push(0xa1);
        break;

      case 'data':
        out.push(args[0].val);
      break;

      default:
        printError('Got an invalid instruction with type \'' + instruction.opcode + '\'', instruction.lineNumber);
    }
  });

  return new Uint8Array(out);
}


function printError(message, lineNumber) {
  console.error('Line ' + lineNumber + ': ' + message);
}

function pushAddress(opcodeMask, address, out) {
  out.push(opcodeMask | (address >> 8));
  out.push(address & 0xFF);
}

function isValidLabel(label) {
  return label.length > 0;
}

function getAddress(address, symbols) {
  if (address.toString().startsWith('$')) {
    return symbols[address.substring(1)];
  }
  return address;
}

function INSTR(opcode, args, lineNumber) {
  return { opcode: opcode, args: (args || []), lineNumber: lineNumber };
}

function ADDR(val) {
  if (!val.startsWith('$')) {
    val = Util.parseNumber(val);
  }
  return val === null ? null : { type: 'address', val: val };
}

function REG(val) {
  if (!val.startsWith('v')) {
    return null;
  }
  var num = parseInt(val.substring(1), 16);
  if (isNaN(num) || num < 0 || num > 15) {
    return null;
  }
  return { type: 'register', val: num };
}

function BYTE(val) {
  val = Util.parseNumber(val);
  if (val === null || val < 0 || val > 255) {
    return null;
  }
  return { type: 'byte', val: val };
}

function NIBBLE(val) {
  val = Util.parseNumber(val);
  if (val === null || val < 0 || val > 15) {
    return null;
  }
  return { type: 'nibble', val: val };
}

function DT(val) {
  return val !== 'dt' ? null : { type: 'dt' };
}

function ST(val) {
  return val !== 'st' ? null : { type: 'st' };
}

function BCD(val) {
  return val !== 'b' ? null : { type: 'bcd', val: val };
}

function FONT(val) {
  return val !== 'f' ? null : { type: 'font' }
}

function KEY(val) {
  return val !== 'k' ? null : { type: 'key' };
}

function I(val) {
  return val !== 'i' ? null : { type: 'i' };
}

function IRANGE(val) {
  return val !== '[i]' ? null : { type: 'irange' };
}

function VO(val) {
  return val !== 'v0' ? null : { type: 'v0' };
}

function DATA(val) {
  val = Util.parseNumber(val);
  if (val === null || val < 0 || val > 255) {
    return null;
  }
  return { type: 'data', val: val };
}


module.exports = AsmCompiler;
