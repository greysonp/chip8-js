require('../css/index.scss');
var saveAs = require('file-saver').saveAs;
var Cpu = require('./Cpu');
var Chip8 = require('./Chip8');
var LcdRenderer = require('./renderers/LcdRenderer');
var KeyboardInput = require('./inputs/KeyboardInput');
var SimpleSpeaker = require('./speakers/SimpleSpeaker');
var CodeEditor = require('./editor/AceEditor');
var HexCompiler = require('./compilers/HexCompiler');
var AsmCompiler = require('./compilers/AsmCompiler');
var SpriteEditor = require('./editor/SpriteEditor');
var SpriteGallery = require('./editor/SpriteGallery');
var DataStore = require('./data/DataStore');
var StorageConstants = require('./data/StorageConstants');

var chip8;
var codeEditor;

function init() {
  var dataStore = new DataStore();

  // Initialize the Chip8 system
  chip8 = new Chip8(
    new Cpu(),
    new LcdRenderer(document.getElementById('game-renderer')),
    new KeyboardInput(document),
    new SimpleSpeaker());

  // Initialize the code editor
  codeEditor = new CodeEditor(document.getElementById('editor-container'), new AsmCompiler(), dataStore);

  // Add click listener to compile button
  document.getElementById('compile-btn').addEventListener('click', function() {
    runGame(codeEditor.getProgram());
  });

  // Add click listener to save button
  document.getElementById('save-btn').addEventListener('click', function() {
    saveGame(codeEditor.getText());
  });

  // Add click listener to export button
  document.getElementById('export-btn').addEventListener('click', function() {
    exportGame(codeEditor.getProgram());
  });

  // Add click listener to load button
  document.getElementById('load-btn').addEventListener('change', loadGameFromFile, false);

  // Initialize the sprite editor
  var spriteCanvas = document.getElementById('sprite-editor');
  var spriteInput = document.getElementById('sprite-editor-input');
  spriteEditor = new SpriteEditor(spriteCanvas, spriteInput, new LcdRenderer(spriteCanvas, {
    width: 8,
    height: 15,
    fade: 1
  }), dataStore);

  // Initialize the sprite gallery
  var spriteGalleryContainer = document.getElementById('sprite-gallery-container');
  var spriteGallery = new SpriteGallery(spriteGalleryContainer, new LcdRenderer.Factory({
    width: 8,
    height: 15,
    fade: 1
  }), dataStore);

  // Load local storage if we have it. If not, load the default template.
  var fileBody = localStorage.getItem(StorageConstants.FILE_BODY);
  if (fileBody) {
    codeEditor.setText(fileBody);
    runGame(codeEditor.getProgram());
  } else {
    loadGame('./src/games/DefaultTemplate.c8');
  }
}

function loadGame(filepath) {
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (request.readyState == 4 && request.status == 200) {
      codeEditor.setText(request.responseText);
      runGame(codeEditor.getProgram());
    }
  }
  request.open('GET', filepath, true);
  request.send(null);
}

function saveGame(contents) {
  var file = new File([contents], 'game.c8.txt', {type: 'text/plain;charset=utf-8'});
  saveAs(file);
}

function exportGame(bytes) {
  var file = new File([bytes], 'game.c8', {type: 'application/octet-stream'});
  saveAs(file);
}

function loadGameFromFile(e) {
  if (e.target.files.length == 0) {
    return;
  }
  var file = e.target.files[0];
  var reader = new FileReader();
  reader.onload = function() {
    codeEditor.setText(reader.result);
    e.target.value = '';
  }
  reader.readAsText(file);
}

function runGame(compiledGame) {
  chip8.reset();
  chip8.load(compiledGame);
  chip8.start();
}

init();
