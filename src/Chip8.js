var Chip8 = function(cpu, renderer, input, speaker) {
  this.cpu = cpu;
  this.renderer = renderer;
  this.input = input;
  this.speaker = speaker;
  this.timerId = 0;
}

Chip8.prototype.start = function() {
  var that = this;
  this.timerId = setInterval(function() {
    // Run the CPU
    for (var i = 0; i < 100; i++) {
      that.cpu.cycle();
    }

    // Play sounds
    if (that.cpu.soundTimer > 0) {
      that.speaker.start();
    } else {
      that.speaker.stop();
    }

    // Handle input and video buffers
    that.cpu.setInputState(that.input.getInputState());
    that.renderer.draw(that.cpu.getVideoBuffer());

    // Update the CPU's 60hz timers
    that.cpu.updateTimers();
  }, 16);
}

Chip8.prototype.load = function(program) {
  this.cpu.load(program);
}

Chip8.prototype.reset = function() {
  clearInterval(this.timerId);
  this.input.reset();
  this.renderer.reset();
  this.cpu.reset();
}

module.exports = Chip8;
