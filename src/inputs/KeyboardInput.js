var INPUT_MAP = {
  49: 1,  // 1 -> 1
  50: 2,  // 2 -> 2
  51: 3,  // 3 -> 3
  52: 12, // 4 -> C
  81: 4,  // Q -> 4
  87: 5,  // W -> 5
  69: 6,  // E -> 6
  82: 13, // R -> D
  65: 7,  // A -> 7
  83: 8,  // S -> 8
  68: 9,  // D -> 9
  70: 14, // F -> E
  90: 10, // Z -> A
  88: 0,  // X -> 0
  67: 11, // C -> B
  86: 15  // V -> F
}

var KeyboardInput = function(document) {
  var that = this;
  document.addEventListener('keydown', function(e) {
    that._onKeyDown(e)
  });
  document.addEventListener('keyup', function(e) {
    that._onKeyUp(e);
  });

  this.input = new Array(16);
}

KeyboardInput.prototype.reset = function() {
  for (var i = 0; i < this.input.length; i++) {
    this.input[i] = false;
  }
}

KeyboardInput.prototype.getInputState = function() {
  return this.input;
}

KeyboardInput.prototype._onKeyDown = function(e) {
  var index = INPUT_MAP[e.keyCode];
  if (typeof index != 'undefined') {
    this.input[index] = true;
  }
}

KeyboardInput.prototype._onKeyUp = function(e) {
  var index = INPUT_MAP[e.keyCode];
  if (typeof index != 'undefined') {
    this.input[index] = false;
  }
}

module.exports = KeyboardInput;
