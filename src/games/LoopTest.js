module.exports = new Uint8Array([
  0x12, 0x03, // jump to 0x203
  0x80,       // x_______
  0x60, 0x01, // set v0 to 1
  0xf0, 0x15, // set the delay timer to v0 (1)
  0xf1, 0x07, // copy the delay timer into v1
  0x31, 0x00, // skip the next instruction if v1 is 0 (meaning the delay timer was 0)
  0x12, 0x07, // jump back up two lines (keeps us in a loop until the delay timer is 0)
  0x00, 0xe0, // clear the screen before drawing
  0xa2, 0x02, // set I to 0x202 (starting address of our sprite)
  0xd2, 0x21, // draw sprite at (v2, v2) that is 1 rows tall
  0x12, 0x03  // jump to 0x203 (start of the program)
]);
