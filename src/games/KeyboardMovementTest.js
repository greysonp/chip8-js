module.exports = new Uint8Array([
  0x12, 0x0a, // jump to 0x20a
  0xfe,       // xxxxxxx_
  0x82,       // x_____x_
  0xaa,       // x_x_x_x_
  0x82,       // x_____x_
  0xaa,       // x_x_x_x_
  0x92,       // x__x__x_
  0x82,       // x_____x_
  0xfe,       // xxxxxxx_
  0x66, 0x01, // set v6 to 1 (for subtracting later)
  0x62, 0x05, // set v2 to 5 (up)
  0x63, 0x07, // set v3 to 8 (left)
  0x64, 0x08, // set v4 to 9 (down)
  0x65, 0x09, // set v5 to 10 (right)
  0x68, 0x02, // set v8 to 2 (giving a framerate of 30fps)
  0xf8, 0x15, // set the delay timer to v8 (1)
  0xf7, 0x07, // copy the delay timer into v7
  0x37, 0x00, // skip the next instruction if v7 is 0 (meaning the delay timer was 0)
  0x12, 0x18, // jump back up two lines (keeps us in a loop until the delay timer is 0)
  0xe2, 0xa1, // skip next instruction if key in v2 isn't pressed
  0x81, 0x65, // subtract 1 from v1
  0xe3, 0xa1, // skip next instruction if key in v3 isn't pressed
  0x80, 0x65, // subtract 1 from v0
  0xe4, 0xa1, // skip next instruction if key in v4 isn't pressed
  0x71, 0x01, // add 1 to v1
  0xe5, 0xa1, // skip next instruction if key in v5 isn't pressed
  0x70, 0x01, // add 1 to v0
  0xa2, 0x02, // set I to 0x202 (starting address of our sprite)
  0x00, 0xe0, // clear the screen before drawing
  0xd0, 0x18, // draw sprite at (v0, v1) that is 8 rows tall
  0x12, 0x00  // jump to 0x200 (start of the program)
]);
