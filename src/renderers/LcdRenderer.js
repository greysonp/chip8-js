var Util = require('../util/Util');

var WIDTH = 64;
var HEIGHT = 32;
var PADDING = 0.08;
var FADE = 0.33;
var PIXEL_COLOR = Util.hexToRgb('#5e6629');
var BKG_COLOR = Util.hexToRgb('#e1e3d8');

var LcdRenderer = function(canvas, options) {
  options = options || {};
  this.WIDTH = options.width || WIDTH;
  this.HEIGHT = options.height || HEIGHT;
  this.PADDING = options.padding || PADDING;
  this.FADE = options.fade || FADE;
  this.PIXEL_COLOR = options.pixelColor || PIXEL_COLOR;
  this.BKG_COLOR = options.bkgColor || BKG_COLOR;

  this.canvas = canvas;
  this.brightness = Util.fillArray(new Array(this.WIDTH * this.HEIGHT), 0);
  this.filteredBuffer = Util.fillArray(new Array(this.WIDTH * this.HEIGHT), 0);
}

LcdRenderer.prototype.reset = function() {
  this.brightness = Util.fillArray(new Array(this.WIDTH * this.HEIGHT), 0);
  this.filteredBuffer = Util.fillArray(new Array(this.WIDTH * this.HEIGHT), 0);
  this.draw(Util.fillArray(new Array(this.WIDTH * this.HEIGHT), false));
}

LcdRenderer.prototype.draw = function(buffer) {
  var ctx = this.canvas.getContext('2d');

  var pwidth = this.canvas.width / (this.WIDTH + this.WIDTH * this.PADDING);
  var pheight = this.canvas.height / (this.HEIGHT + this.HEIGHT * this.PADDING);
  var hpadding = pwidth * this.PADDING;
  var vpadding = pheight * this.PADDING;

  // Clear the screen
  ctx.fillStyle = this._getFillStyle(this.BKG_COLOR);
  ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

  // Draw all of the pixels
  this.brightness.forEach(function(oldBrightness, i, array) {
    var newBrightness = 0;

    // Fade out pixels that aren't active in the video buffer anymore, and max out the brightness on the pixels that are
    if (buffer[i]) {
      newBrightness = 1;
    } else if (array[i] > 0) {
      newBrightness = Math.max(oldBrightness - this.FADE, 0);
    }
    array[i] = newBrightness;

    // Draw the pixel on the canvas
    ctx.fillStyle = this._fillStyleAtBrightness(newBrightness);
    var x = i % this.WIDTH;
    var y = Math.floor(i / this.WIDTH);
    ctx.fillRect(
      (pwidth * x) + (hpadding * x),
      (pheight * y) + (vpadding * y),
      pwidth - hpadding,
      pheight - vpadding
    );
  }, this);
}

LcdRenderer.prototype._fillStyleAtBrightness = function (brightness) {
  // Invert the brightness for math purposes
  brightness = 1 - brightness;

  // The color at the target brightness level
  var actual = [0, 0, 0];

  actual.forEach((function(val, i) {
    var diff = this.PIXEL_COLOR[i] - this.BKG_COLOR[i];
    var brightnessFactor = diff * brightness;
    actual[i] = Math.round(this.PIXEL_COLOR[i] - brightnessFactor);
  }).bind(this));

  return this._getFillStyle(actual);
}

LcdRenderer.prototype._getFillStyle = function (colorArray) {
  return 'rgb(' + colorArray[0] + ', ' + colorArray[1] + ', ' + colorArray[2] +')';
};

LcdRenderer.Factory = function(options) {
  this.options = options;
}

LcdRenderer.Factory.prototype.build = function(canvas) {
  return new LcdRenderer(canvas, this.options);
}

module.exports = LcdRenderer;
