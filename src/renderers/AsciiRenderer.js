var WIDTH = 64;
var HEIGHT = 32;

var AsciiRenderer = function(containerElement) {
  var pre = document.createElement('pre');
  containerElement.appendChild(pre);
  this.element = pre;
}

AsciiRenderer.prototype.reset = function() {
  this.draw(util.fillArray(new Array(WIDTH * HEIGHT), false));
}

AsciiRenderer.prototype.draw = function(buffer) {
  var output = '';
  for (var i = 0; i < buffer.length; i++) {
    if (i % WIDTH == 0) {
      output += '\n';
    }
    output += buffer[i] ? 'X' : '.';
  }
  this.element.innerText = output;
}

module.exports = AsciiRenderer;
