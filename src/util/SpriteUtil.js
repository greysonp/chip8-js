var Util = require('./Util');

var SPRITE_START = '// BEGIN SPRITES';
var SPRITE_END = '// END SPRITES';

function textToSprites(text) {
  var sprites = {};
  var rows = text.split('\n');
  var i = 0;
  var row = '';
  var label = '';
  var data = '';

  for (i = 0; i < rows.length; i++) {
    row = rows[i];

    // We found a label
    if (row.indexOf(':') === 0) {
      label = row.substr(1);

      // Get all the data for the label
      i++;
      while (i < rows.length && rows[i].indexOf(':') !== 0 && rows[i].trim().length > 0) {
        data += rows[i] + '\n';
        i++;
      }
      i--;
      sprites[label] = data;
      data = '';
    }
  }
  return sprites;
}

function spritesToText(sprites) {
  var output = '';
  Object.keys(sprites).forEach(function (key) {
    output += ':' + key + '\n';
    output += sprites[key];
  });
  return output;
}

function extractSpriteTextFromCode(code) {
  var data = getCodeMetadata(code);
  
  var spriteRows = data.codeRows.slice(data.startIndex, data.endIndex + 1);
  return spriteRows.join('\n');
}

function replaceSpriteTextInCode(code, spriteText) {
  var data = getCodeMetadata(code);
  var spriteRows = spriteText.split('\n');
  
  data.codeRows.splice(data.startIndex, data.endIndex - data.startIndex + 1);
  for (var i = 0; i < spriteRows.length; i++) {
    data.codeRows.splice(data.startIndex + i, 0, spriteRows[i]);
  }
  var val = data.codeRows.join('\n');
  return val;
}

function getCodeMetadata(code) {
  var rows = code.split('\n');
  var startIndex = 0;
  var endIndex = rows.length;

  for (var i = 0; i < rows.length; i++) {
    if (rows[i] === SPRITE_START) {
      startIndex = i;
    } else if (rows[i] === SPRITE_END) {
      endIndex = i;
      break;
    }
  }

  // Offset the indices so that we don't include the start/end indicator
  startIndex++;
  endIndex--;

  return {
    codeRows: rows,
    startIndex: startIndex,
    endIndex: endIndex
  };
}

function fillBufferWithSpriteData(buffer, data) {
  // Convert data to list of bools
  var rows = data.split('\n');
  rows = rows.map(function(e) {
    var val = Util.parseNumber(e.trim());
    var bools = [];
    for (var i = 0; i < 8; i++) {
      bools.push((val & 0b10000000) > 0);
      val = val << 1;
    }
    return bools;
  });
  
  // Put data in buffer
  Util.fillArray(buffer, false);
  rows.forEach((function(e, index) {
    for (var i = 0; i < e.length; i++) {
      buffer[index * e.length + i] = e[i];
    }
  }).bind(this));
}

var SpriteUtil = {
  textToSprites: textToSprites,
  spritesToText: spritesToText,
  extractSpriteTextFromCode: extractSpriteTextFromCode,
  replaceSpriteTextInCode: replaceSpriteTextInCode,
  fillBufferWithSpriteData: fillBufferWithSpriteData
};

module.exports = SpriteUtil;