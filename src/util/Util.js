function fillArray(array, state) {
  for (var i = 0; i < array.length; i++) {
    array[i] = state;
  }
  return array;
}

function parseNumber(byte) {
  var out = NaN;
  if (byte.startsWith('0b')) {
    out = parseInt(byte.substring(2), 2);
  } else if (byte.startsWith('0x')) {
    out = parseInt(byte, 16);
  } else {
    out = parseInt(byte);
  }
  return isNaN(out) ? null : out;
}

function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? [
    parseInt(result[1], 16),
    parseInt(result[2], 16),
    parseInt(result[3], 16)
  ] : null;
}

var Util = {
  fillArray: fillArray,
  parseNumber: parseNumber,
  hexToRgb: hexToRgb
}

module.exports = Util;
