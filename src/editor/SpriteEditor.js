require('../../css/sprite-editor.scss');
var Util = require('../util/Util');
var SpriteUtil = require('../util/SpriteUtil');

var WIDTH = 8;
var HEIGHT = 15;

var SpriteEditor = function(canvas, input, renderer, dataStore) {
  this.canvas = canvas;
  this.input = input;
  this.renderer = renderer;
  this.dataStore = dataStore;
  this.name = 'smiley';
  this.input.value = this.name;
  this.ignoreSpriteChanged = false;

  // Add listeners
  this.canvas.addEventListener('mousedown', this._onMouseDown.bind(this));
  this.dataStore.addEventListener(this.dataStore.SPRITES_CHANGED, this._onSpritesChanged.bind(this));
  this.dataStore.addEventListener(this.dataStore.ACTIVE_SPRITE_CHANGED, this._onActiveSpriteChanged.bind(this));
  this.input.addEventListener('keyup', this._onTextChanged.bind(this));
  this.input.addEventListener('onchange', this._onTextChanged.bind(this));
  window.addEventListener('resize', this._onWindowSizeChanged.bind(this));
  this._onWindowSizeChanged();
  
  // Initialize buffer
  this.buffer = [];
  for (var i = 0; i < WIDTH * HEIGHT; i++) {
    this.buffer[i] = false;
  }
}

SpriteEditor.prototype._onMouseDown = function(e) {
  // Can't do anything if we don't have a sprite to draw to
  if (!this.name) {
    return;
  }

  // Set pixel in buffer
  var x = Math.floor((e.offsetX / this.canvas.clientWidth) * WIDTH);
  var y = Math.floor((e.offsetY / this.canvas.clientHeight) * HEIGHT);
  this.buffer[coordToArray(x, y)] ^= true;

  // Draw
  this._drawPixels();

  // Update datastore
  this.ignoreSpriteChanged = true;
  this.dataStore.putSprite(this.name, this._convertToSpriteText());
  this.ignoreSpriteChanged = false;
}

SpriteEditor.prototype._onWindowSizeChanged = function(e) {
  var scaledWidth = this.canvas.width * (this.canvas.clientHeight / this.canvas.height);
  this.canvas.style.width = scaledWidth + 'px';
  this.canvas.parentElement.style.width = scaledWidth + 'px';
}

SpriteEditor.prototype._onTextChanged = function(e) {
  var newName = this.input.value;
  this.ignoreSpriteChanged = true;
  this.dataStore.renameSprite(this.name, newName);
  this.ignoreSpriteChanged = false;
  this.name = newName;
}

SpriteEditor.prototype._onActiveSpriteChanged = function(e) {
  this.name = this.dataStore.getActiveSprite();
  this.input.value = this.name;
  this._onSpritesChanged();
}

SpriteEditor.prototype._onSpritesChanged = function(e) {
  if (this.ignoreSpriteChanged) {
    return;
  }

  // Handle a simple rename
  if (e && e.action == this.dataStore.ACTION_SPRITE_RENAMED) {
    if (e.oldName === this.name) {
      this.name = e.newName;
      this.input.value = this.name;
      return;
    }
  }

  // If we have no data, clear the canvas and leave
  var data = this.dataStore.getSprite(this.name);
  if (!data) {
    Util.fillArray(this.buffer, false);
    this._drawPixels();
    return;
  }

  SpriteUtil.fillBufferWithSpriteData(this.buffer, data);

  this._drawPixels();
}

SpriteEditor.prototype._drawPixels = function() {
  this.renderer.draw(this.buffer);
}

SpriteEditor.prototype._convertToSpriteText = function() {
  var lastEmptyIndex = this.buffer.length - 1;
  for (var i = this.buffer.length - 1; i >= 0; i--) {
    if (this.buffer[i]) {
      break;
    }
    lastEmptyIndex = i;
  }
  var spriteHeight = Math.ceil(lastEmptyIndex / WIDTH);
  
  var output = '';
  for (var y = 0; y < spriteHeight; y++) {
    var row = '  0b';  
    for (var x = 0; x < WIDTH; x++) {
      if (this.buffer[coordToArray(x, y)]) {
        row += '1';
      } else {
        row += '0';
      }
    }
    output += row + '\n';
  }

  return output;
}

function coordToArray(x, y) {
  return (y * WIDTH) + x;
}

function nameToLabel(name) {
  return ':' + name;
}

module.exports = SpriteEditor;
