require('../../css/text-editor.scss');
var SpriteUtil = require('../util/SpriteUtil');

var TextEditor = function(containerElement, compiler, dataStore) {
  this.compiler = compiler;
  this.dataStore = dataStore;
  this.editor = document.createElement('textarea');
  this.editor.spellcheck = false;
  containerElement.appendChild(this.editor);

  this.editor.addEventListener('keyup', this._onTextChanged.bind(this));
  this.editor.addEventListener('onchange', this._onTextChanged.bind(this));
  this.dataStore.addEventListener(dataStore.SPRITES_CHANGED, this._onSpritesChanged.bind(this));
  this.ignoreTextChanged = false;
  this.ignoreSpriteChanged = false;
}

TextEditor.prototype.getProgram = function() {
  return this.compiler.compile(this.editor.value);
}

TextEditor.prototype.getText = function() {
  return this.editor.value;
}

TextEditor.prototype.setText = function(text) {
  this.editor.value = text;
  this.dataStore.setSprites(
    SpriteUtil.textToSprites(
      SpriteUtil.extractSpriteTextFromCode(this.getText())
    )
  );
}

TextEditor.prototype._onTextChanged = function(e) {
  if (this.ignoreTextChanged) {
    return;
  }
  this.ignoreSpriteChanged = true;
  this.dataStore.setSprites(
    SpriteUtil.textToSprites(
      SpriteUtil.extractSpriteTextFromCode(this.getText())
    )
  );
  this.ignoreSpriteChanged = false;
}

TextEditor.prototype._onSpritesChanged = function(e) {
  if (this.ignoreSpriteChanged) {
    return;
  }
  var cursor = this.editor.selectionStart;
  this.ignoreTextChanged = true;
  this.setText(SpriteUtil.replaceSpriteTextInCode(
    this.getText(), 
    SpriteUtil.spritesToText(this.dataStore.getSprites())
  ));
  this.ignoreTextChanged = false;
  this.editor.selectionStart = this.editor.selectionEnd = cursor;
  // this.editor.selection.moveTo(cursor.row, cursor.column);
}

module.exports = TextEditor;
