require('../../css/sprite-gallery.scss');
var SpriteUtil = require('../util/SpriteUtil');

var SpriteGallery = function(container, rendererFactory, dataStore) {
  this.container = container;
  this.rendererFactory = rendererFactory;
  this.dataStore = dataStore;
  this.items = {};

  this.dataStore.addEventListener(this.dataStore.SPRITES_CHANGED, this._onSpritesChanged.bind(this));
  window.addEventListener('resize', this._onWindowSizeChanged.bind(this));
}

SpriteGallery.prototype._onSpritesChanged = function(e) {
  this._rebuild();
}

SpriteGallery.prototype._rebuild = function(e) {
  this.container.innerHTML = '';

  var sprites = this.dataStore.getSprites();

  // Add the 'new' button to the front of the list
  var newButton = new SpriteGalleryNewButton();
  var btnElem = newButton.getElement();
  btnElem.addEventListener('click', this._onNewSpriteButtonClicked.bind(this));
  this.container.appendChild(btnElem);

  // Add all of the sprites to the list
  Object.keys(sprites).forEach((function(key) {
    var galleryItem = new SpriteGalleryItem(key, this.rendererFactory);
    galleryItem.draw(sprites[key]);
    var elem = galleryItem.getElement();
    elem.addEventListener('click', this._onGalleryItemClicked.bind(this));
    this.container.appendChild(elem);

    this.items[key] = galleryItem;
  }).bind(this));

  this._onWindowSizeChanged();  
}

SpriteGallery.prototype._onGalleryItemClicked = function(e) {
  var key = e.target.getAttribute('data-key');
  this.dataStore.setActiveSprite(key);
}

SpriteGallery.prototype._onNewSpriteButtonClicked = function(e) {
  var name = prompt('What label do you want to give your new sprite?');
  if (name.trim().length == 0) {
    return;
  }

  if (this.dataStore.getSprite(name)) {
    alert('That name is already taken.');
    return;
  }
  
  this.dataStore.putSprite(name, '0b00000000');
  this.dataStore.setActiveSprite(name);
}

SpriteGallery.prototype._onWindowSizeChanged = function(e) {
  var btn = document.getElementsByClassName('new-btn')[0];
  if (btn) {
    btn.style.width = (btn.clientHeight / 2) + 'px';
  }
}

// ============================================================
// SpriteGalleryItem
// ============================================================

var SpriteGalleryItem = function(key, rendererFactory) {
  this.element = this._makeElement(key);
  this.renderer = rendererFactory.build(this.element);

  this.WIDTH = 8;
  this.HEIGHT = 15;

  // Initialize buffer
  this.buffer = [];
  for (var i = 0; i < this.WIDTH * this.HEIGHT; i++) {
    this.buffer[i] = false;
  }
}

SpriteGalleryItem.prototype.getElement = function() {
  return this.element;
}

SpriteGalleryItem.prototype.draw = function(data) {
  SpriteUtil.fillBufferWithSpriteData(this.buffer, data);
  this.renderer.draw(this.buffer);
}

SpriteGalleryItem.prototype._makeElement = function(key) {
  var canvas = document.createElement('canvas');
  canvas.width = 40;
  canvas.height = 75;
  canvas.className = 'gallery-item';
  canvas.setAttribute('data-key', key);
  return canvas;
}

// ============================================================
// SpriteGalleryNewButton
// ============================================================

var SpriteGalleryNewButton = function() {
  this.element = this._makeElement();
}

SpriteGalleryNewButton.prototype.getElement = function() {
  return this.element;
}

SpriteGalleryNewButton.prototype._makeElement = function() {
  var div = document.createElement('div');
  div.className = 'gallery-item new-btn';

  var table = document.createElement('div');
  table.className = 'table';

  var cell = document.createElement('div');
  cell.className = 'cell';

  div.appendChild(table);
  table.appendChild(cell);
  cell.innerText = '+'
  return div;
}

module.exports = SpriteGallery;