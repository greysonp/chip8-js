var SpriteUtil = require('../util/SpriteUtil');
var StorageConstants = require('../data/StorageConstants');
require('brace');
require('brace/mode/assembly_x86');
require('brace/theme/solarized_light');
require('../../css/ace-theme.scss');

var CodeEditor = function(containerElement, compiler, dataStore) {
  this.compiler = compiler;
  this.dataStore = dataStore;
  this.editor = ace.edit(containerElement);
  this.editor.setOptions({
    fontFamily: 'code',
    fontSize: '14pt'
  });

  // God knows how to add our own custom theme when using the 'brace' npm package, so instead
  // we just have to pull some hackery to overwrite rules in the assembly mode
  this.editor.getSession().setMode('ace/mode/assembly_x86', (function() {
    var mode = this.editor.getSession().getMode();
    mode.$highlightRules.$rules = chipRules;
    var newMode = Object.create(Object.getPrototypeOf(mode));
    Object.assign(newMode, mode);
    newMode.$tokenizer = null;
    newMode.lineCommentStart = '//';
    this.editor.getSession().$onChangeMode(newMode);
  }).bind(this));
  this.editor.setTheme('ace/theme/solarized_light');
  this.editor.$blockScrolling = Infinity
  this.editor.getSession().setUseSoftTabs(true);
  this.editor.getSession().setTabSize(2);
  this.editor.on('change', this._onTextChanged.bind(this));
  this.dataStore.addEventListener(dataStore.SPRITES_CHANGED, this._onSpritesChanged.bind(this));
  this.ignoreTextChanged = false;
  this.ignoreSpriteChanged = false;
}

CodeEditor.prototype.getProgram = function() {
  return this.compiler.compile(this.editor.getValue());
}

CodeEditor.prototype.getText = function() {
  return this.editor.getValue();
}

CodeEditor.prototype.setText = function(text) {
  this.editor.setValue(text, -1);
}

CodeEditor.prototype._onTextChanged = function(e) {
  // Save the file to local storage
  localStorage.setItem(StorageConstants.FILE_BODY, this.getText());

  if (this.ignoreTextChanged) {
    return;
  }
  this.ignoreSpriteChanged = true;
  this.dataStore.setSprites(
    SpriteUtil.textToSprites(
      SpriteUtil.extractSpriteTextFromCode(this.getText())
    )
  );
  this.ignoreSpriteChanged = false;
}

CodeEditor.prototype._onSpritesChanged = function(e) {
  if (this.ignoreSpriteChanged) {
    return;
  }
  var cursor = this.editor.getCursorPosition();
  this.ignoreTextChanged = true;
  this.setText(SpriteUtil.replaceSpriteTextInCode(
    this.getText(), 
    SpriteUtil.spritesToText(this.dataStore.getSprites())
  ));
  this.ignoreTextChanged = false;
  this.editor.selection.moveTo(cursor.row, cursor.column);
}

var chipRules = { start:
        [ { token : "empty_line",
            regex : '^$'},
          { token: 'keyword.control.assembly',
            regex: '\\b(cls|ret|jp|call|se|sne|ld|or|and|xor|add|sub|shr|subn|shl|rnd|drw|skp|sknp)\\b',
            caseInsensitive: true },
          { token: 'variable.parameter.register.assembly',
            regex: '\\b(v[A-F0-9])\\b',
            caseInsensitive: true },
          { token: 'variable.parameter.special.assembly',
            regex: '\\b(dt|st|i|b|f)\\b',
            caseInsensitive: true },
          { token: 'constant.character.decimal.assembly',
            regex: '\\b[0-9]+\\b' },
          { token: 'constant.character.hexadecimal.assembly',
            regex: '\\b0x[A-F0-9]+\\b',
            caseInsensitive: true },
          { token: 'constant.character.binary.assembly',
            regex: '\\b0b[0-1]+\\b',
            caseInsensitive: true },
          { token: 'entity.name.function.assembly', regex: '^\\s*:\\w+' },
          { token: 'entity.name.function.assembly', regex: '\\$\\w+' },
          { token: 'comment.assembly', regex: '//.*$' },
          { defaultToken : "text" } ]
      };

module.exports = CodeEditor;
